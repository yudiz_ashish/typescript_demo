// Why Type Script 
// -----> Provide an optional type system for javascript

// Example 1 :

// var foo = 123;
// console.log(foo);
// foo = '123';


// var foo : number = 123;
// // var foo : number = '123';
// console.log(foo);

// Example For Class

//Simple Class

// class A {
//     x:number;
//     y:number;

//     constructor(x:number,y:number){
//         this.x = x;
//         this.y = y;
//         console.log("Value of X = ",this.x);
//         console.log("Value of Y = ",this.y);
//     }

//     sum():number{
//         return this.x + this.y;
//     }

//     division():number {
//         return this.x / this.y;
//     }

//     mul():string {
//         return "Multiplication of X * Y"+this.x * this.y;
//     }
// }

// var a = new A(10,20);
// console.log(a.sum());
// console.log(a.division());
// console.log(a.mul());



// Super Class

// class A {
//     constructor(){
//         console.log("Class A");
//     }   
// }

// class B extends A {
//     constructor(){
//         console.log("Class B");
//         super();
//     }
// }

// var b = new B();



//Generic in Typescript 
//Example 1 

// function demoString (x:string) : string {
//     return x;
// }
// console.log(demoString("Hello TypeScript"));

// function demoNumber(x:number) : number{
//     return x;
// }
// console.log(demoNumber(10));

// function demo<T>(x:T):T {
//     return x;
// }  

// console.log("Hellow Typescript");
// console.log(10);



//Example 2

// class Queue {
//     data:Array<number> = [];
//     push(item:number){
//         this.data.push(item);
//     }

//     pop(){
//         this.data.shift();
//     }

//     getList():number[]{
//         return this.data;
//     }
// }

// const queue = new Queue();
// queue.push(10);
// queue.push(20);
// queue.push(30);
// // queue.push("40");
// console.log("Queue List");
// console.log(queue.getList());
// console.log("Queue List");
// queue.pop();
// console.log(queue.getList());



// class Queue<T>{
//     data:Array<T> = [];
//     push(item:T){
//         this.data.push(item);
//     }
//     pop(){
//         this.data.shift();
//     }
//     getList():T[]{
//         return this.data;
//     }
// }

// const queue = new Queue<number>();
// queue.push(10);
// queue.push(20);
// queue.push(30);
// console.log("Queue List");
// console.log(queue.getList());
// console.log("Queue List");
// queue.pop();
// console.log(queue.getList())

// const q = new Queue<string>();
// q.push('Chirag');
// q.push('Dollly');
// q.push('Hina');
// console.log("Queue List");
// console.log(q.getList());
// console.log("Queue List");
// q.pop();
// console.log(q.getList())
function getProperty<T, K extends keyof T>(obj: T, key: K) {
    return obj[key];  // Inferred type is T[K]
}

function setProperty<T, K extends keyof T>(obj: T, key: K, value: T[K]) {
    obj[key] = value;
}

let x = { foo: 10, bar: "hello!" };

let foo = getProperty(x, "foo"); // number
let bar = getProperty(x, "bar"); // string
console.log(foo);